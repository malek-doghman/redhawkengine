cmake_minimum_required (VERSION 3.0)

project (red_hawk_engine)

add_subdirectory(RayTracingEngine/src)

add_executable (red_hawk_engine
    "RayTracingEngine/Camera.cpp"
    "RayTracingEngine/HitableContainer.cpp"
    "RayTracingEngine/IHitable.cpp"
    "RayTracingEngine/Sphere.cpp"
    "RayTracingEngine/src/core/src/utility.cpp"
    "RayTracingEngine/main.cpp"
)

target_link_libraries(red_hawk_engine
    PRIVATE
        core
        material
)

target_compile_options(red_hawk_engine PUBLIC -std=c++2a -pedantic -Wall -g)
