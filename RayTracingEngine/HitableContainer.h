#pragma once

#include <vector>

#include "IHitable.h"
#include "material/HitRecord.h"

namespace redhawk
{
	class HitableContainer :
		public IHitable
	{
	public:
		HitableContainer() = default;
		virtual ~HitableContainer();

		void AddHitableObject(IHitable* obj);

		virtual bool Hit(const Ray& ray,
						 float t_min,
						 float t_max,
			             HitRecord& record) const;

	private:
		std::vector<IHitable*> hitables_;
	};
}