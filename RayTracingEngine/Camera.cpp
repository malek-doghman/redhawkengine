#include "Camera.h"

namespace redhawk
{
	Camera::Camera(vec3 lookfrom, vec3 lookat, vec3 viewup, float aspect, float vfov)
	{
		float theta = vfov * 3.14f / 180;
		float half_h = tanf(theta / 2);
		float half_w = aspect * half_h;
		float z_plane = -1.0f;
		origin_ = lookfrom;
		vec3 direction = lookfrom - lookat;
		direction = direction.make_unit_vector();
		vec3 u = vec3::cross(direction, viewup).make_unit_vector();
		vec3 v = vec3::cross(direction, u);

		//lower_left_corner_ = redhawk::vec3(-half_w, -half_h, z_plane);
		lower_left_corner_ = origin_ - u * half_w - v * half_h - direction;
		horizontal_ = u * 2 * half_w;
		vertical_ = v * 2 * half_h;
	}

	Ray Camera::GetRay(float u, float v)
	{
		return Ray(origin_, lower_left_corner_ - origin_ + horizontal_ * u + vertical_ * v);
	}
}