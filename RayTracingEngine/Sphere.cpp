#include <math.h>
#include "Sphere.h"
namespace redhawk
{
	Sphere::Sphere(Material* material) : IHitable(material)
	{
	}

	Sphere::Sphere(vec3 center, float radius, Material* material)
		: IHitable(material), center_(center), radius_(radius)
	{
	}

	bool Sphere::Hit(const Ray& ray, float t_min, float t_max, HitRecord& record) const
	{
		redhawk::vec3 oc = ray.origin() - center_;
		float a = ray.direction().dot(ray.direction());
		float b = 2.0f * oc.dot(ray.direction());
		float c = oc.dot(oc) - radius_ * radius_;
		float discriminant = b * b - 4 * a * c;
		if (discriminant > 0)
		{
			float root1 = (-b - sqrtf(discriminant)) / (2.0f * a);
			float root2 = (-b + sqrtf(discriminant)) / (2.0f * a);

			if (root1 < t_max && root1 > t_min)
			{
				record.t = root1;
				record.p = ray.point_at_parameter(root1);
				record.normal = (record.p - center_) / radius_;
				record.material_ptr = material_ptr_;
				return true;
				//vec3 color = vec3(sphere_normal.x() + 1, sphere_normal.y() + 1, sphere_normal.z() + 1) * 0.5f;
			}
			if (root2 < t_max && root2 > t_min)
			{
				record.t = root2;
				record.p = ray.point_at_parameter(root2);
				record.normal = (record.p - center_) / radius_;
				record.material_ptr = material_ptr_;
				return true;
			}
		}
		return false;
	}
}