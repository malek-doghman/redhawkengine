#include "HitableContainer.h"

namespace redhawk
{
	HitableContainer::~HitableContainer()
	{
		for (size_t i = 0; i < hitables_.size(); i++)
		{
			delete hitables_[i];
		}
	}

	void HitableContainer::AddHitableObject(IHitable* obj)
	{
		hitables_.push_back(obj);
	}

	bool HitableContainer::Hit(const Ray& ray, float t_min, float t_max, HitRecord& record) const
	{
		HitRecord tmp_record;
		float closest_so_fat = t_max;
		bool hit_anything = false;
		for (size_t i = 0; i < hitables_.size(); i++)
		{
			if (hitables_[i]->Hit(ray, t_min, closest_so_fat, tmp_record)) {
				hit_anything = true;
				closest_so_fat = tmp_record.t;
				record = tmp_record;
			}
		}
		return hit_anything;
	}
}