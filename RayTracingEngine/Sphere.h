#pragma once
#include "IHitable.h"
#include "core/vec3.h"
namespace redhawk
{
	class Sphere :
		public IHitable
	{
	public:
		Sphere(Material* material);
		Sphere(vec3 center, float radius, Material* material);
		~Sphere() = default;

		virtual bool Hit(const Ray& ray, float t_min, float t_max, HitRecord& record) const;

	private:
		vec3 center_;
		float radius_ = 0.0f;
	};
}