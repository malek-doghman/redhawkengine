#pragma once
#include "core/vec3.h"
namespace redhawk {
	namespace utility
	{
		float random();
		vec3 random_in_unit_sphere();
		vec3 Reflect(const vec3& I, const vec3& N);
		bool Refract(const vec3& I, const vec3& N, float ni_over_nt, vec3& refracted);
		float Schlick(float cosine, float ref_idx);
	}
}