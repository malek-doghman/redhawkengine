#pragma once
#include <math.h>
#include <stdlib.h>
namespace redhawk
{
	class vec3
	{
	public:
		vec3() = default;
		vec3(float e1, float e2, float e3);
		~vec3() = default;

		inline float x() { return e[0]; }
		inline float y() { return e[1]; }
		inline float z() { return e[2]; }

		inline float r() { return e[0]; }
		inline float g() { return e[1]; }
		inline float b() { return e[2]; }

		inline vec3 operator-() const { return vec3(-e[0], -e[1], -e[2]); }
		inline float operator[](int i) const { return e[i]; }
		inline float& operator[](int i) { return e[i]; }

		inline void operator += (const vec3& v2);
		inline void operator -= (const vec3& v2);
		inline void operator *= (const vec3& v2);
		inline void operator /= (const vec3& v2);

		friend inline vec3 operator + (const vec3& v1, const vec3& v2) {
			return vec3(
				v1.e[0] + v2.e[0],
				v1.e[1] + v2.e[1],
				v1.e[2] + v2.e[2]
			);
		}

		friend inline vec3 operator - (const vec3& v1, const vec3& v2) {
			return vec3(
				v1.e[0] - v2.e[0],
				v1.e[1] - v2.e[1],
				v1.e[2] - v2.e[2]
			);
		}

		friend inline vec3 operator * (const vec3& v1, const vec3& v2) {
			return vec3(
				v1.e[0] * v2.e[0],
				v1.e[1] * v2.e[1],
				v1.e[2] * v2.e[2]
			);
		}

		inline vec3 operator * (float t) const { return vec3(e[0] * t, e[1] * t, e[2] * t); }

		inline vec3 operator / (float t) const { return vec3(e[0] / t, e[1] / t, e[2] / t); }

		friend inline vec3 operator / (const vec3& v1, const vec3& v2) {
			return vec3(
				v1.e[0] / v2.e[0],
				v1.e[1] / v2.e[1],
				v1.e[2] / v2.e[2]
			);
		}

		inline vec3 operator / (float t) { return vec3(e[0] / t, e[1] / t, e[2] / t); }

		inline float dot(const vec3& v1) const { return v1.e[0] * e[0] + v1.e[1] * e[1] + v1.e[2] * e[2]; }

		inline static vec3 cross(const vec3& v1, const vec3& v2) {
			return vec3(
				v1.e[1] * v2.e[2] - v1.e[2] * v2.e[1],
				v1.e[0] * v2.e[2] - v1.e[2] * v2.e[0],
				v1.e[0] * v2.e[1] - v1.e[1] * v2.e[0]
			);
		}

		inline float lenght() const { return sqrtf(e[0] * e[0] + e[1] * e[1] + e[2] * e[2]); }
		inline float squared_lenght() { return e[0] * e[0] + e[1] * e[1] + e[2] * e[2]; }
		inline vec3 make_unit_vector() const { return *this / this->lenght(); }
	private:
		float e[3];
	};
}