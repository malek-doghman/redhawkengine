#pragma once
#include <vector>
#include <string>

#include "core/vec3.h"

namespace redhawk
{
	class mat
	{
	public:
		mat() = default;
		~mat() = default;
		mat(int w, int h);

		void AddPixel(const vec3& pix);
		void ModifyPixel(const vec3& pix, size_t x, size_t y);
		void WriteToPPM(std::string filename);

	private:
		std::vector<vec3> data_;
		int w_ = 0;
		int h_ = 0;
	};
}