#pragma once

#include "core/vec3.h"

namespace redhawk
{
	class Ray
	{
	public:
		Ray() = default;
		Ray(const vec3& a, const vec3& b);
		~Ray() = default;
		vec3 origin() const;
		vec3 direction() const;
		vec3 point_at_parameter(float t) const;
	private:
		vec3 origin_;
		vec3 direction_;
	};
}
