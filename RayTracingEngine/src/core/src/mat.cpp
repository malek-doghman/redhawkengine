#include <fstream>
#include "core/mat.h"

namespace redhawk
{
	mat::mat(int w, int h)
	{
		w_ = w;
		h_ = h;
		data_.reserve(w * h);
	}

	void mat::AddPixel(const vec3& pix)
	{
		data_.push_back(pix);
	}
	void mat::ModifyPixel(const vec3& pix, size_t x, size_t y)
	{
		size_t index = x * w_ + y;
		if (index < data_.size())
		{
			data_[index] = pix;
		}
	}
	void mat::WriteToPPM(std::string filename)
	{
		std::ofstream stream(filename);
		stream << "P3" << std::endl;
		stream << w_ << " " << h_ << std::endl;
		stream << 255 << std::endl;
		for (size_t i = 0; i < data_.size(); i++)
		{
			int r = static_cast<int>(255 * data_[i].r());
			int g = static_cast<int>(255 * data_[i].g());
			int b = static_cast<int>(255 * data_[i].b());
			stream << r << " " << g << " " << b << std::endl;
		}
	}
}