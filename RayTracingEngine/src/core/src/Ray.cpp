#include "core/Ray.h"

namespace redhawk
{
	Ray::Ray(const vec3& a, const vec3& b)
	{
		origin_ = a;
		direction_ = b;
	}

	vec3 Ray::origin() const
	{
		return origin_;
	}

	vec3 Ray::direction() const
	{
		return direction_;
	}

	vec3 Ray::point_at_parameter(float t) const
	{
		return origin_ + direction_ * t;
	}
}