#include "core/utility.h"
namespace redhawk {
	namespace utility {
		float random() {
			return ((float)rand() / (RAND_MAX)) * 0.99f; // 0 <= r < 1 
		}

		vec3 random_in_unit_sphere() {
			redhawk::vec3 p;
			do
			{
				p = redhawk::vec3(random(), random(), random()) * 2.0 - redhawk::vec3(1.0f, 1.0f, 1.0f);
			} while (p.dot(p) >= 1.0);
			return p;
		}

		vec3 Reflect(const vec3& I, const vec3& N)
		{
			return I - N * I.dot(N) * 2;
		}

		bool Refract(const vec3& I, const vec3& N, float ni_over_nt, vec3& refracted)
		{
			vec3 ui = I.make_unit_vector();
			float theta = ui.dot(N);
			float root = 1.0f - ni_over_nt * ni_over_nt * (1.0f - theta * theta);
			if (root > 0)
			{
				refracted = (I - N * theta) * ni_over_nt - N * sqrtf(root);
				return true;
			}
			return false;
		}

		float Schlick(float cosine, float ref_idx)
		{
			float r0 = (1.0f - ref_idx) / (1.0f + ref_idx);
			r0 = r0 * r0;
			return r0 + (1 - r0) * powf(1.0f - cosine, 5);
		}
	}
}