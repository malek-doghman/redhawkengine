#include "core/vec3.h"

namespace redhawk
{
	vec3::vec3(float e0, float e1, float e2)
	{
		e[0] = e0;
		e[1] = e1;
		e[2] = e2;
	}

	inline void vec3::operator+=(const vec3& v2)
	{
		this->e[0] += v2.e[0];
		this->e[1] += v2.e[1];
		this->e[2] += v2.e[2];
	}
	inline void vec3::operator-=(const vec3& v2)
	{
		this->e[0] -= v2.e[0];
		this->e[1] -= v2.e[1];
		this->e[2] -= v2.e[2];
	}
	inline void vec3::operator*=(const vec3& v2)
	{
		this->e[0] *= v2.e[0];
		this->e[1] *= v2.e[1];
		this->e[2] *= v2.e[2];
	}
	inline void vec3::operator/=(const vec3& v2)
	{
		this->e[0] /= v2.e[0];
		this->e[1] /= v2.e[1];
		this->e[2] /= v2.e[2];
	}
}