add_library(core
	STATIC
		src/mat.cpp
		src/vec3.cpp
		src/Ray.cpp
		src/utility.cpp
)

target_include_directories(core
	PUBLIC
		include
)