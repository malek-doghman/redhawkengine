#pragma once
#include "material/Material.h"

namespace redhawk
{
	class Metal :
		public Material
	{
	public:
		Metal(vec3 albedo, float fuzz);
		~Metal() = default;
		bool Scatter(const Ray& in, const HitRecord& record, vec3& attenuation, Ray& scattered) const;
	private:
		vec3 albedo_;
		float fuzz_ = 0.0f;
	};
}