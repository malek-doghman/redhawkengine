#pragma once
#include "material/Material.h"
namespace redhawk
{
	class Dielectric :
		public Material
	{
	public:
		Dielectric(float ri);
		~Dielectric() = default;
		virtual bool Scatter(const Ray& in, const HitRecord& record, vec3& attenuation, Ray& scattered) const;
	private:
		float ref_idx_;
	};
}