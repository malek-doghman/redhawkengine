#pragma once

#include "core/vec3.h"

namespace redhawk
{
	class Material;

	struct HitRecord
	{
		float t;
		vec3 p;
		vec3 normal;
		Material* material_ptr;
	};
}