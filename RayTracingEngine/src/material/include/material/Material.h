#pragma once

#include "core/Ray.h"
#include "material/HitRecord.h"

namespace redhawk
{
	class Material
	{
	public:
		Material() = default;
		~Material() = default;
		virtual bool Scatter(const Ray& in, const HitRecord& record, vec3& attenuation, Ray& scattered) const = 0;
	};
}