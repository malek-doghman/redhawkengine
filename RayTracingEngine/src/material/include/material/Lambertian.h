#pragma once
#include "material/Material.h"
namespace redhawk
{
	class Lambertian :
		public Material
	{
	public:
		Lambertian(vec3 albedo);
		~Lambertian() = default;
		bool Scatter(const Ray& in, const HitRecord& record, vec3& attenuation, Ray& scattered) const;
	private:
		vec3 albedo_;
	};
}
