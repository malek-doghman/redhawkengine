#include "material/Metal.h"
#include "core/utility.h"
namespace redhawk
{
	Metal::Metal(vec3 albedo, float fuzz)
	{
		albedo_ = albedo;
		if (fuzz < 1)
		{
			fuzz_ = fuzz;
		}
	}

	bool Metal::Scatter(const Ray& in, const HitRecord& record, vec3& attenuation, Ray& scattered) const
	{
		vec3 reflected = utility::Reflect(in.direction().make_unit_vector(), record.normal);
		scattered = Ray(record.p, reflected + utility::random_in_unit_sphere() * fuzz_);
		attenuation = albedo_;
		return (scattered.direction().dot(record.normal) > 0);
	}
}