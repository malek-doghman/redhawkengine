#include "material/Lambertian.h"
#include "core/utility.h"
namespace redhawk
{
	Lambertian::Lambertian(vec3 albedo)
	{
		albedo_ = albedo;
	}

	bool Lambertian::Scatter(const Ray& in, const HitRecord& record, vec3& attenuation, Ray& scattered) const
	{
		redhawk::vec3 target = record.p + record.normal + utility::random_in_unit_sphere();
		scattered = Ray(record.p, target - record.p);
		attenuation = albedo_;
		return true;
	}
}