#include "material/Dielectric.h"
#include "core/utility.h"

namespace redhawk
{
	Dielectric::Dielectric(float ri)
	{
		ref_idx_ = ri;
	}

	bool Dielectric::Scatter(const Ray& in, const HitRecord& record, vec3& attenuation, Ray& scattered) const
	{
		attenuation = vec3(1.0f, 1.0f, 1.0f);
		float ni_over_nt = ref_idx_;
		vec3 outward_normal = -record.normal;
		float cosine = ref_idx_ * in.direction().make_unit_vector().dot(record.normal);
		if (in.direction().dot(record.normal) < 0)
		{
			outward_normal = record.normal;
			ni_over_nt = 1.0f / ref_idx_;
			cosine = -in.direction().make_unit_vector().dot(record.normal);
		}

		vec3 refracted;
		float reflect_prob = 1.0f;
		vec3 reflected = utility::Reflect(in.direction(), record.normal);
		if (utility::Refract(in.direction(), outward_normal, ni_over_nt, refracted))
		{
			reflect_prob = utility::Schlick(cosine, ref_idx_);
		}
		if (utility::random() >= reflect_prob)
		{
			scattered = Ray(record.p, refracted);
		}
		else {
			scattered = Ray(record.p, reflected);
		}
		return true;
	}
}