#include "IHitable.h"

namespace redhawk
{
	IHitable::IHitable(Material* material)
	{
		material_ptr_ = material;
	}

	IHitable::~IHitable()
	{
		delete material_ptr_;
	}
}