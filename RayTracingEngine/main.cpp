#include <time.h>
#include <limits>

#include "core/vec3.h"
#include "core/mat.h"
#include "core/Ray.h"
#include "Sphere.h"
#include "HitableContainer.h"
#include "Camera.h"
#include "core/utility.h"
#include "material/Material.h"
#include "material/Lambertian.h"
#include "material/Metal.h"
#include "material/Dielectric.h"

//juste to create an image with nice color into it
redhawk::mat CreateMat(size_t nx = 200, size_t ny = 100) {
	redhawk::mat im(nx, ny);
	for (size_t i = 0; i < nx; i++)
	{
		for (size_t j = 0; j < ny; j++)
		{
			float r = float(i) / float(nx);
			float g = float(j) / float(ny);
			float b = 0.2f;
			im.AddPixel(redhawk::vec3(r, g, b));
		}
	}
	return im;
}

int render = 1;
redhawk::vec3 Color(const redhawk::Ray& r, const redhawk::HitableContainer& container, int depth) {
	redhawk::HitRecord record;
	if (container.Hit(r, 0.001f, std::numeric_limits<float>::max(), record))
	{
		if (render == 1)
		{
			redhawk::vec3 att;
			redhawk::Ray scattered;
			if (depth < 50 && record.material_ptr->Scatter(r, record, att, scattered))
			{
				return att * Color(scattered, container, depth + 1);
			}
			else {
				return redhawk::vec3(0.0f, 0.0f, 0.0f);
			}
		}
		else {
			return redhawk::vec3(record.normal.x() + 1, record.normal.y() + 1, record.normal.z() + 1) * 0.5f;
		}
	}
	redhawk::vec3 unit_direction = r.direction().make_unit_vector(); // -1.0 < y << 1.0
	float t = 0.5f * (unit_direction.y() + 1.0f); // scaling t so that 0.0 < t < 0.1
	//Lerp Formula: blended_value = (1-t) * start_value + t * end_value;
	return redhawk::vec3(1.0f, 1.0f, 1.0f) * (1.0f - t) + redhawk::vec3(0.5f, 0.7f, 1.0f) * t;
}

void PopulateScene(redhawk::HitableContainer& scene) {
	for (int a = -3; a < 3; a++)
	{
		for (int b = -3; b < 3; b++)
		{
			redhawk::vec3 center(a + redhawk::utility::random(), 0.0f, b + redhawk::utility::random());
			srand(time(NULL));
			redhawk::vec3 color(redhawk::utility::random(), redhawk::utility::random(), redhawk::utility::random());
			scene.AddHitableObject(new redhawk::Sphere(center, 0.4f, new redhawk::Lambertian(color)));
		}
	}

	redhawk::Sphere* main = new redhawk::Sphere(redhawk::vec3(0.0f, -100.5f, -1.0f), 100.0f, new redhawk::Lambertian(redhawk::vec3(0.8f, 0.3f, 0.0f)));
	redhawk::Sphere* lambertian = new redhawk::Sphere(redhawk::vec3(0.0f, 0.0f, -1.0f), 0.5f, new redhawk::Lambertian(redhawk::vec3(0.8f, 0.3f, 0.3f)));
	redhawk::Sphere* metallic = new redhawk::Sphere(redhawk::vec3(1.0f, 0.0f, -1.0f), 0.5, new redhawk::Metal(redhawk::vec3(1.0f, 0.2f, 0.2f), 0.1));
	redhawk::Sphere* glass = new redhawk::Sphere(redhawk::vec3(-1.0f, 0.0f, -1.0f), -0.5, new redhawk::Dielectric(1.5));

	scene.AddHitableObject(main);
	scene.AddHitableObject(lambertian);
	scene.AddHitableObject(glass);
	scene.AddHitableObject(metallic);
}

redhawk::mat LinearInterpolation() {
	redhawk::HitableContainer scene;
	PopulateScene(scene);

	size_t nx = 1000;
	size_t ny = 500;
	size_t n_sample = 100;

	redhawk::mat im(nx, ny);

	redhawk::vec3 lookfrom(0.0f, 1.0f, 5.5f);
	redhawk::vec3 lookat(0.0f, 0.0f, -1.0f);
	redhawk::vec3 viewup(0.0f, 1.0f, 0.0f);
	redhawk::Camera camera(lookfrom, lookat, viewup, nx * 1.0f / ny, 75);

	for (size_t j = ny; j > 0; j--)
	{
		for (size_t i = 0; i < nx; i++)
		{
			redhawk::vec3 color(0.0f, 0.0f, 0.0f);
			for (size_t s = 0; s < n_sample; s++)
			{
				float u = float(i + redhawk::utility::random()) / float(nx);
				float v = float(j + redhawk::utility::random()) / float(ny);
				redhawk::Ray ray = camera.GetRay(u, v);
				color = color + Color(ray, scene, 0);
			}
			color = color / float(n_sample);
			color = redhawk::vec3(sqrtf(color[0]), sqrtf(color[1]), sqrtf(color[2]));
			im.AddPixel(color);
		}
	}
	return im;
}
int main() {
	redhawk::mat im = LinearInterpolation();
	im.WriteToPPM("ch7.ppm");
	return 0;
}