#pragma once

#include "core/Ray.h"
#include "material/HitRecord.h"

namespace redhawk
{
	class Material;

	class IHitable
	{
	public:
		IHitable() = default;
		IHitable(Material* material);
		virtual ~IHitable();
		virtual bool Hit(const Ray& ray, float t_min, float t_max, HitRecord& record) const = 0;
	protected:
		Material* material_ptr_ = nullptr; // TODO use smart ptr
	};
}
