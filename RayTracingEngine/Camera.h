#pragma once
#include "core/Ray.h"
#include "core/vec3.h"

namespace redhawk
{
	class Camera
	{
	public:
		Camera(vec3 lookfrom, vec3 lookat, vec3 viewup, float aspect, float vfov = 90.0f);
		~Camera() = default;
		Ray GetRay(float u, float v);
	private:
		vec3 lower_left_corner_;
		vec3 horizontal_;
		vec3 vertical_;
		vec3 origin_;
	};
}